let startBtn = document.getElementById("start");
let text = document.getElementById('text');

let timer = {
    clock: false,
    time: 0
};

function reset() {
    timer.clock = false;
    timer.time = 0;
    text.innerHTML = `00:00.0`
    startBtn.setAttribute("value", "start");

}

function StartTimer() {
    if (timer.clock === false) {
        timer.clock = true;
        run()
        startBtn.setAttribute("value", "pause");
    } else {
        timer.clock = false;
        startBtn.setAttribute("value", "start");

    }

}

function run() {
    if (timer.clock === true) {
        setTimeout(function () {
            let milisec = timer.time % 10;
            let sec = Math.floor(timer.time / 10 % 60);
            let min = Math.floor(timer.time / 10 / 60);
            timer.time++
            if (sec < 10) {
                sec = `0${sec}`
            };
            if (min < 10) {
                min = `0${min}`
            };
            text.innerHTML = `${min}:${sec}.${milisec}`
            run()
        }, 100);
    }
}